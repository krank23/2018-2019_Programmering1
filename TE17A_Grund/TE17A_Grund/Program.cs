﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE17A_Grund
{
    class Program
    {
        static void Main(string[] args)
        {

            string name = "Micke";
            Console.WriteLine("Skriv in ditt namn: ");
            name = Console.ReadLine();

            Console.WriteLine("Hello, " + name);

            Console.ReadLine();
        }
    }
}
