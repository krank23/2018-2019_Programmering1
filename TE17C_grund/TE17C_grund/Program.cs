﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE17C_grund
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Name? ");

            string name = Console.ReadLine();

            // name = "Herbert";

            Console.WriteLine("Hello, " + name + "!");

            Console.ReadLine();
        }
    }
}
