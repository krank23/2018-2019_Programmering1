﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE17B_Grund
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.ForegroundColor = ConsoleColor.Cyan;
            //string name = "Micke";

            //name = "Anti-Micke" + " Bergström";

            // Fråga användaren om namn
            Console.Write("Skriv ditt namn: ");
            string name = Console.ReadLine();

            // Skriv ut namnet
            Console.WriteLine("Hello " + name + "!");
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
